import React from 'react';
import Layout from '../components/Layout';

const AboutPage: React.FC = () => {
  return (
    <Layout>
      <div>
        <h1>About Us</h1>
        <p>This is the about page.</p>
      </div>
    </Layout>
  );
};

export default AboutPage;
