import React from 'react';
import Layout from '../components/Layout';

const ContactPage: React.FC = () => {
  return (
    <Layout>
      <div>
        <h1>Contact Us</h1>
        <p>This is the contact page.</p>
      </div>
    </Layout>
  );
};

export default ContactPage;
