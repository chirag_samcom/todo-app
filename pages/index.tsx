import React from 'react';
import Layout from '../components/Layout';

const HomePage: React.FC = () => {
  return (
    <Layout>
      <div>
        <h2>Welcome to Home Page!</h2>
      </div>
    </Layout>
  );
};

export default HomePage;
